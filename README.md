Command line flags:
-postalCode=
-houseNumber=
-addition=
-mqttClientId=                  <optional>
-mqttUri="broker.local:1883"
-mqttSchema="ssl" (or "tcp")    <optional>
-mqttUser=
-mqttPassword=
-mqttTopic="afvalwijzer"        <optional>