package main

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

const afvalwijzerUrl = "https://json.mijnafvalwijzer.nl/?method=postcodecheck&postcode=%v&huisnummer=%v&toevoeging=%v"

type Flags struct {
	postalCode string
	houseNumber string
	addition string
	mqttClientId string
	mqttUri string
	mqttSchema string
	mqttUser string
	mqttPwd string
	mqttTopic string
}

type Message struct {
	Response string `json:"response"`
	Data     struct {
		Ophaaldagen struct {
			Response string      `json:"response"`
			Data     []Data      `json:"data"`
			Error    interface{} `json:"error"`
		} `json:"ophaaldagen"`
	} `json:"data"`
	Error interface{} `json:"error"`
}

type Data struct {
	NameType string `json:"nameType"`
	Type     string `json:"type"`
	Date     string `json:"date"`
}

func main() {
	//get flag settings
	flags := getFlags()

	ophaaldagen, err := getOphaaldagen(flags.postalCode, flags.houseNumber, flags.addition)

	if err != nil {
		log.Fatal(err)
	}

	upcoming, err := upcomingOphaaldagen(ophaaldagen, 5)

	if err != nil {
		log.Fatal(err)
	}

	//fmt.Println(upcoming)

	client, err := mqttConnect(flags.mqttClientId, flags.mqttUri, flags.mqttSchema, flags.mqttUser, flags.mqttPwd)

	if err != nil {
		log.Fatal(err)
	}

	publishMessage(client, upcoming, flags.mqttTopic)
	client.Disconnect(2000)

}

func getFlags() Flags {
	postalCode := flag.String("postalCode", "", "Postal Code")
	houseNumber := flag.String("houseNumber", "", "House number")
	addition := flag.String("addition", "", "House number addition")
	mqttClientId := flag.String("mqttClientId", "Afvalwijzer MQTT Client", "MQTT Client ID")
	mqttUri := flag.String("mqttUri", "", "URI for MQTT broker")
	mqttSchema := flag.String("mqttSchema", "ssl", "Schema for connection with the broker")
	mqttUser := flag.String("mqttUser", "afvalwijzer", "User ID for MQTT connection")
	mqttPwd := flag.String("mqttPassword", "", "Password for MQTT connection")
	mqttTopic := flag.String("mqttTopic", "afvalwijzer", "Topic to publish message to")

	flag.Parse()

	flags := Flags{
		postalCode: *postalCode,
		houseNumber: *houseNumber,
		addition: *addition,
		mqttClientId: *mqttClientId,
		mqttUri: *mqttUri,
		mqttSchema: *mqttSchema,
		mqttUser: *mqttUser,
		mqttPwd: *mqttPwd,
		mqttTopic: *mqttTopic,
	}

	return flags
}

func getOphaaldagen(postalCode string, houseNumber string, addition string) ([]Data, error) {
	//Get Data
	urlSet := fmt.Sprintf(afvalwijzerUrl, postalCode, houseNumber, addition)
	//fmt.Println(urlSet)
	tr := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}

	//check if proxy is configured
	proxyEnv := os.Getenv("HTTP_PROXY")
	if proxyEnv != "" {
		proxyUrl, _ := url.Parse(proxyEnv)
		tr.Proxy = http.ProxyURL(proxyUrl)
	}

	client := &http.Client{Transport: tr}
	resp, err := client.Get(urlSet)

	if err != nil {
		return nil, err
	}

	// Read body
	b, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}

	// Unmarshal
	var msg Message
	err = json.Unmarshal(b, &msg)
	if err != nil {
		return nil, err
	}

	// Check data correct
	if msg.Response != "OK" {
		return nil, errors.New(fmt.Sprint(msg.Error))
	}
	if msg.Data.Ophaaldagen.Response != "OK" {
		return nil, errors.New(fmt.Sprint(msg.Data.Ophaaldagen.Error))
	}

	return msg.Data.Ophaaldagen.Data, nil
}

func upcomingOphaaldagen(ophaaldagen []Data, numberOfDays int) ([]Data, error) {
	now := time.Now()
	zone, _ := now.Zone()
	for i, ophaaldag := range ophaaldagen {
		dt, _ := time.Parse("2006-01-02 MST", fmt.Sprintf("%s %s", ophaaldag.Date, zone))
		if dt.After(now) {
			return ophaaldagen[i:i + numberOfDays], nil
		}
	}

	return nil, errors.New("No upcoming ophaaldagen to return")
}


func mqttConnect(clientId string, uri string, schema string,  user string, passwd string) (mqtt.Client, error) {
	opts := mqttCreateClientOptions(clientId, uri, schema, user, passwd)
	client := mqtt.NewClient(opts)
	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
	}
	if err := token.Error(); err != nil {
		return nil, err
	}
	return client, nil
}

func mqttCreateClientOptions(clientId string, uri string, schema string, user string, passwd string) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("%s://%s",schema, uri))
	opts.SetUsername(user)
	opts.SetPassword(passwd)
	opts.SetClientID(clientId)
	tlsConf := &tls.Config{
		InsecureSkipVerify: true,
	}
	opts.SetTLSConfig(tlsConf)

	return opts
}

func publishMessage(client mqtt.Client, data []Data, topic string) {
	topicNext := fmt.Sprintf("%s/pickup/next", topic)

	//publish topics
	topicStatus := fmt.Sprintf("%s/status", topicNext)
	tkn := client.Publish(topicStatus, 0, true, strconv.Itoa(daysTillNextPickup(data)))
	tkn.Wait()
	topicDate := fmt.Sprintf("%s/date", topicNext)
	tkn = client.Publish(topicDate, 0, true, data[0].Date)
	tkn.Wait()
	topicType := fmt.Sprintf("%s/type", topicNext)
	tkn = client.Publish(topicType, 0, true, typesNextPickup(data))
	tkn.Wait()
}

func daysTillNextPickup(data []Data) int {
	//calculate days till pickup
	zone, _ := time.Now().Zone()
	pickup, _ := time.Parse("2006-01-02 MST", fmt.Sprintf("%s %s", data[0].Date, zone))
	diff := time.Until(pickup)
	days := (int(diff.Hours()) / 24) + 1

	return days
}

func typesNextPickup(data []Data) string {
	date := data[0].Date
	nameType := data[0].NameType

	for i := 1; i < len(data); i++ {
		if date != data[i].Date	{ break }

		nameType += " | " + data[i].NameType
	}

	return nameType
}